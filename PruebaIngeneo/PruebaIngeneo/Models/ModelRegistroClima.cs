﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaIngeneo.Models
{
    public class ModelRegistroClima
    {

        public int id_registro { get; set; }
        public string grados { get; set; }
        public DateTime? fecha { get; set; }
        public string nombreciudad { get; set; }
    }
}
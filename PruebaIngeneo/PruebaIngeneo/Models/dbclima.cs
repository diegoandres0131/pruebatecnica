namespace PruebaIngeneo.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbclima : DbContext
    {
        public dbclima()
            : base("name=dbclima")
        {
        }

        public virtual DbSet<Ciudad> Ciudad { get; set; }
        public virtual DbSet<loginUsuario> loginUsuario { get; set; }
        public virtual DbSet<RegistroClima> RegistroClima { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ciudad>()
                .Property(e => e.nombreciudad)
                .IsUnicode(false);

            modelBuilder.Entity<Ciudad>()
                .HasMany(e => e.RegistroClima)
                .WithRequired(e => e.Ciudad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<loginUsuario>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<loginUsuario>()
                .Property(e => e.clave)
                .IsUnicode(false);

            modelBuilder.Entity<RegistroClima>()
                .Property(e => e.grados)
                .IsUnicode(false);
        }
    }
}

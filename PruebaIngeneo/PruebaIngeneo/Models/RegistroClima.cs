namespace PruebaIngeneo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RegistroClima")]
    public partial class RegistroClima
    {
        [Key]
        public int id_registro { get; set; }

        [StringLength(40)]
        public string grados { get; set; }

        public DateTime? fecha { get; set; }

        public int id_ciudad { get; set; }

        public virtual Ciudad Ciudad { get; set; }
    }
}

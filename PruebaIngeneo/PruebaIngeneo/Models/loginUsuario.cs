namespace PruebaIngeneo.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("loginUsuario")]
    public partial class loginUsuario
    {
        [Key]
        public int id_login { get; set; }

        [StringLength(200)]
        public string usuario { get; set; }

        [StringLength(200)]
        public string clave { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using PruebaIngeneo.Models;

namespace PruebaIngeneo
{
    /// <summary>
    /// Descripción breve de WServicioClima
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WServicioClima : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public List<ModelRegistroClima> ConsultarClima()
        {


            List<ModelRegistroClima> Registros;

            using (dbclima db = new dbclima())
            {

                Registros = (from d in db.RegistroClima
                             join e in db.Ciudad on d.id_ciudad equals e.id_ciudad
                             select new ModelRegistroClima
                             {

                                 id_registro = d.id_registro,
                                 grados = d.grados,
                                 fecha = d.fecha,
                                 nombreciudad = e.nombreciudad,
                             }).ToList();

            }


            return Registros;
        }

        [WebMethod]
        public string eliminarRegistro(int id)
        {
            try
            {
                // TODO: Add delete logic here
                using (dbclima db = new dbclima())
                {

                    var eliminar = db.RegistroClima.Find(id);

                    db.Entry(eliminar).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }


                return "true";

            }
            catch
            {
                return "Ocurrio un error al eliminar";
            }

        }

        [WebMethod]
        public string ActualizarRegistro(int id, string grados, DateTime fecha, int id_ciudad)
        {
            try
            {
                // TODO: Add delete logic here
                using (dbclima db = new dbclima())
                {

                    var actualizar = db.RegistroClima.Find(id);

                    actualizar.grados = grados;
                    actualizar.fecha = fecha;
                    actualizar.id_ciudad = id_ciudad;
                    db.Entry(actualizar).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }


                return "true";

            }
            catch
            {
                return "Ocurrio un error al Actualizar";
            }

        }

        [WebMethod]
        public string InsertarRegistro(string grados, DateTime fecha, int id_ciudad)
        {
            try
            {
                // TODO: Add delete logic here
                using (dbclima db = new dbclima())
                {

                    var registrar = new RegistroClima();

                    registrar.grados = grados;
                    registrar.fecha = fecha;
                    registrar.id_ciudad = id_ciudad;
                    db.Entry(registrar).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }


                return "true";

            }
            catch
            {
                return "Ocurrio un error al Registrar";
            }

        }

        [WebMethod(EnableSession = true)]

        public string Login(string user, string clave)
        {
            string claveEncriptada = WServicioClima.GetMD5(clave);
            using (dbclima db = new dbclima())
            {
                var obj = db.loginUsuario.Where(a => a.usuario.Equals(user) && a.clave.Equals(claveEncriptada)).FirstOrDefault();
                if (obj != null)
                {
                    var pname = "si";
                    Session["Nombre"] = pname;

                    System.Web.HttpContext.Current.Session["Id_Usuario"] = obj.id_login.ToString();
                    System.Web.HttpContext.Current.Session["usuario"] = obj.usuario;
                    return "true";
                }
                return "true";
            }

            /*  List<LoginModel> usuario;
              // TODO: Add delete logic here
              using (dbclima db = new dbclima())
              {

                  usuario = (from d in db.loginUsuario
                             where d.usuario.Equals(user) && d.clave.Equals(claveEncriptada)
                             select new LoginModel
                             {
                                 id_login = d.id_login,
                                 usuario = d.usuario,
                             }).ToList();

              }

              var cont = 0;

              foreach (var probar in usuario)
              {
                  if (probar.id_login != null)
                  {
                        cont = cont + 1;
                      Session["id_login"] = probar.id_login.ToString();
                      Session["usuario"] = probar.usuario.ToString();
                      Session["Autenticacion"] = "true";

                  }

              }

              if (cont > 0)
              {
                  return "true";
              }

              return "false";*/
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

    }
}

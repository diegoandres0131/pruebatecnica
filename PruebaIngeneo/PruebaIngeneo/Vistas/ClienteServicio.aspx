﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClienteServicio.aspx.cs" Inherits="PruebaIngeneo.Vistas.ClienteServicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Consumo de servicio</title>
    <style type="text/css">
        #form1 {
            height: 310px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="grados" runat="server" Text="Grados" CssClass="form-control"></asp:Label>
            <asp:TextBox ID="Textgrados" runat="server" CssClass="form-control"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Fecha" runat="server" Text="Fecha"></asp:Label>
            <input type="date" id="inputfecha" runat="server"/>
            <br />
            <br />
            <asp:DropDownList ID="dropciudades" runat="server" AutoPostBack="true">
            </asp:DropDownList>
             <br />
             <br />          
            <asp:Button ID="btnregistrar" runat="server" Text="Registrar" OnClick="btnregistrar_Click" />           
        </div>
        

        <br />
        <br />

        <h1>Datos registrados</h1>


        <asp:GridView ID="GridView1" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" onrowdeleting="GridView1GridView_RowDeleting" >
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Edit" HeaderText="Editar" ShowHeader="True" Text="Editar" />
                <asp:ButtonField ButtonType="Button" CommandName="Delete" HeaderText="Eliminar" ShowHeader="True" Text="Eliminar" />
            </Columns>
        </asp:GridView>

        <br />             

      <!--  <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder> -->

    </form>
</body>
</html>


﻿using PruebaIngeneo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaIngeneo.Vistas
{
    public partial class Login : System.Web.UI.Page
    {

        ServiceReference1.WServicioClimaSoapClient wsclima = new ServiceReference1.WServicioClimaSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            string usuario =  Convert.ToString(this.textusuario.Text);
            string clave = Convert.ToString(this.textclave.Text);
            var probar = wsclima.Login(usuario, clave);
            
               if (probar == "true")
                {
              
                        Response.Redirect("/Vistas/ClienteServicio.aspx");
                    
                }

           
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PruebaIngeneo.Models;

namespace PruebaIngeneo.Vistas
{
    public partial class ClienteServicio : System.Web.UI.Page
    {
        StringBuilder tabla = new StringBuilder();
        ServiceReference1.WServicioClimaSoapClient  wsclima = new ServiceReference1.WServicioClimaSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {

           

            if (Session["Nombre"] != null)
            {
                if (!IsPostBack)
                {

                    CargarCombo();
                    llenartabla();


                }
            }
            else
            {
                Response.Redirect("/Vistas/Login.aspx");
            }


        }

        private void llenartabla()
        {
            var result = wsclima.ConsultarClima();

            this.GridView1.DataSource = result;
            this.GridView1.DataBind();


            /*  tabla.Append("<table border='1'");
              tabla.Append("<tr><th>Id Registro</th><th>Grados</th><th>Fecha</th><th>Ciudad</th><th>Acciones</th>");
              tabla.Append("</tr>");


              if (result != null)
              {

                 foreach(var i in result)
                  {
                      tabla.Append("<tr>");
                      tabla.Append("<td>" + i.id_registro + "</td>");
                      tabla.Append("<td>" + i.grados + "</td>");
                      tabla.Append("<td>" + i.fecha + "</td>");
                      tabla.Append("<td>" + i.nombreciudad + "</td>");
                      tabla.Append("<td> <asp:Button ID="btnregistrar" runat="server" Text="Registrar" OnClick="btnregistrar_Click" />   </td>");
                      tabla.Append("</tr>");
                  }

              }

              tabla.Append("</table>");
              PlaceHolder1.Controls.Add(new Literal { Text = tabla.ToString() });*/
        }


        private void CargarCombo()
        {

            using (dbclima cnn = new dbclima())
            {

                var Ciudades = (from c in cnn.Ciudad
                                select new
                                {
                                    c.id_ciudad,
                                    c.nombreciudad
                                }).ToList();

                dropciudades.DataSource = Ciudades;
                dropciudades.DataValueField = "id_ciudad";
                dropciudades.DataTextField = "nombreciudad";
                dropciudades.DataBind();
                dropciudades.Items.Insert(0, "    --Select--    ");




            }

        }

        protected void btnregistrar_Click(object sender, EventArgs e)
        {
            string grados = Convert.ToString(this.Textgrados.Text);
            DateTime fecha = Convert.ToDateTime(this.inputfecha.Value);
            int id_ciudad = Convert.ToInt32(this.dropciudades.SelectedValue);
            var probar = wsclima.InsertarRegistro(grados, fecha, id_ciudad);

            if (probar == "true")
            {
                Response.Redirect("/Vistas/ClienteServicio.aspx");

            }

        }

        protected void GridView1GridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];

            var id = row.Cells[2].Text;

            var a = GridView1.Rows.Count;


            var probar = wsclima.eliminarRegistro(Convert.ToInt32(id));

            if (probar == "true")
            {
                Response.Redirect("/Vistas/ClienteServicio.aspx");

            }


        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PruebaIngeneo.Vistas.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">

                <asp:Label ID="lblusuario" runat="server" Text="Usuario"></asp:Label>
               <br />
                <asp:TextBox ID="textusuario" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="lblclave" runat="server" Text="Clave"></asp:Label>
                <br />
                <asp:TextBox ID="textclave" type="password" runat="server" Width="164px"></asp:TextBox>
                <br /><br /><br />
                <asp:Button ID="btnEliminar" runat="server" Text="Ingresar" OnClick="btnEliminar_Click" />
            </div>
        </div>
    </form>
</body>
</html>
